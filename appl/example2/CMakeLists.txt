add_input_file_links()

# 1pnc tests
dune_add_test(NAME test_1pnc_uphill_h2
              LABELS porousmediumflow 1p2c
              SOURCES main.cc
              COMPILE_DEFINITIONS FLUIDSYSTEM=FluidSystems::H2N2CO2FluidSystem<TypeTag>)
