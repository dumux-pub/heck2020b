SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

N. Ahmadi, K. Heck, M. Rolle, R. Helmig, K. Mosthaf<br>
On multicomponent gas diffusion and coupling concepts for porous media and free flow:  A benchmark study

Installation with Docker
=======================

The easiest way to install this module is to create a new directory and download the docker image:
```
mkdir heck2020b
cd heck2020b
wget https://git.iws.uni-stuttgart.de/dumux-pub/heck2020b/-/raw/master/docker/docker_heck2020b.sh
```

After that, one may open the docker container by running
```
bash docker_heck2020b.sh open
```
Installation
============

The easiest way to install this module is to create a new directory and clone this module:
```
mkdir heck2020b && cd heck2020b
git clone https://git.iws.uni-stuttgart.de/dumux-pub/heck2020b.git
```

After that, execute the file [installHeck2020b.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Heck2020b/raw/master/installHeck2020b.sh).
```
chmod +x heck2020b/installHeck2020b.sh
./heck2020b/installHeck2020b.sh
```

This should automatically download all necessary modules and check out the correct versions. Afterwards dunecontrol is run.

Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installHeck2020b.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Heck2020b/raw/master/installHeck2020b.sh).


Applications
============

After the script has run successfully, you may build all executables

```bash
cd heck2020b/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake folder according to the following path:

- appl/example1
- appl/example2
- appl/example3
- appl/example4

They can be executed with an input file e.g., by running

```bash
cd appl/example1
./test_darcy1phorizontal_maxwellstefan params.input
```
